function Slide(container, options) {
    this.slider = container;
    this.container = this.slider.children('div');
    this.lists = this.container.children('ul');
    this.options = {
        itemCount       : 5,
        responsive : {
            enabled     : true,
            itemCount   : 3
        },
        nextNum         : 1,
        prevNum         : 1,
        auto : {
            enabled     : true,
            interval    : 3000,
            clickTimeout: 2000
        },
        arrow : {
            isVisible   : true,
            leftIcon    : $('<i>').attr({class: "fa fa-arrow-left", ariaHidden: "true"}),
            rightIcon   : $('<i>').attr({class: "fa fa-arrow-right", ariaHidden: "true"}),
            color       : 'grey',
            width       : 5
        },
        slideDuration   : 500
    }
    this.arrow = {
        left : null,
        right : null
    }
    this.timeout = null;
    this.currentWidth = null;

    this.$left = true;
    this.$right = false;
    this.$mobile = false;
    this.$window = true;

    _.assign(this.options, options);

    this.init();
}
Slide.prototype.init = function() {
    let self = this;
    let width = $( window ).width();

    self.render();

    if (self.options.auto.enabled) {
        self.startAuto();
    }
    if (self.options.arrow.isVisible) {
        let arrowCSS = {
            alignItems      : 'center',
            justifyContent  : 'center',
            display         : 'flex',
            width           : self.options.arrow.width + '%',
            height          : '100%',
            color           : self.options.arrow.color
        }

        self.arrow.left = $('<a>');
        self.arrow.right = $('<a>');

        self.arrow.left.css(arrowCSS);
        self.arrow.right.css(arrowCSS);

        self.arrow.left.hover().css({
            textDecoration : 'none'
        });
        self.arrow.right.hover().css({
            textDecoration : 'none'
        });

        self.options.arrow.leftIcon.appendTo(self.arrow.left);
        self.arrow.left.prependTo(self.slider);

        self.options.arrow.rightIcon.appendTo(self.arrow.right);
        self.arrow.right.appendTo(self.slider);

        self.arrow.left.click(function() {
            self.onClick(self.$left);
        });
        self.arrow.right.click(function() {
            self.onClick(self.$right);
        });
    }
    if (self.options.responsive.enabled) {
        $( window ).resize(function() {
            let width = $( window ).width();

            if (width >= 992) {
                if (!self.currentWidth) {
                    self.resize(self.$window);
                }
            } else {
                if (self.currentWidth) {
                    self.resize(self.$mobile);
                }
            }
        });
    }
}
Slide.prototype.render = function() {
    let self = this;
    let container_width = 100;
    let list_width = (100 / self.options.itemCount) + '%';

    if (self.options.arrow.isVisible) {
        container_width = 100 - (Number.parseInt(self.options.arrow.width) * 2);
    }
    self.container.css({
        width           : container_width + '%',
        height          : '100%',
        overflow        : 'hidden'
    });
    self.lists.css({
        listStyle       : 'none',
        display         : 'flex',
        width           : '100%',
        height          : '100%',
        justifyAlign    : 'center',
        margin          : 0
    });
    self.lists.children('li').css({
        'min-width'     : list_width,
        'max-width'     : list_width
    });
    self.lists.children('li').children('a').css({
        'height'        : '100%',
        'width'         : '100%'
    });

    if (self.options.responsive.enabled) {
        if ($( window ).width() < 992) {
            self.resize(self.$mobile);
        }
    }
}
Slide.prototype.resize = function(size) {
    let list_width;

    if (size) {
        list_width = (100 / this.options.itemCount) + '%';
        this.currentWidth = this.$window;
    } else {
        list_width = (100 / this.options.responsive.itemCount) + '%';
        this.currentWidth = this.$mobile;
    }

    this.lists.children('li').css({
        'min-width'     : list_width,
        'max-width'     : list_width
    });
}
Slide.prototype.startAuto = function() {
    let self = this;

    self.autoSlide = setInterval(function() {
        self.slide(self.$right);
    }, (self.options.auto.interval + self.options.slideDuration));
}
Slide.prototype.stopAuto = function() {
    let self = this;

    clearInterval(self.autoSlide);
    self.autoSlide = null;
}
Slide.prototype.onClick = function(direction) {
    let self = this;

    self.stopAuto();

    if (self.timeout === null) {
        self.timeout = setTimeout(function() {
            self.startAuto();
            self.timeout = null;
        }, self.options.auto.clickTimeout);
    }

    self.slide(direction);
}
Slide.prototype.slide = function(direction) {
    let self = this;
    let count, start;

    let parent_width = Number.parseInt(self.lists.css('width'));
    let child_width = Number.parseInt(self.lists.children(':first').css('width'));
    let percent = (100 / (parent_width / child_width));

    if (direction) {
        count = self.options.nextNum;
        start = '0px';
    } else {
        count = self.options.prevNum;
        start = -percent + '%';
    }

    _.times(count, function() {
        if (direction) {
            self.lists.animate({
                'left': -percent + '%'
            }, 0, 'linear', function() {
                let content = self.lists.children(':last').detach();
                $(content).prependTo(self.lists);
            });
        }

        self.lists.animate({
            left: start
        }, self.options.slideDuration / count, 'linear', function() {
            if (!direction) {
                let content = self.lists.children(':first').detach();
                $(content).appendTo(self.lists);

                self.lists.css('left', '0px');
            }
        });
    });
}
